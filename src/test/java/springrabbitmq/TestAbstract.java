package springrabbitmq;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.RabbitMQContainer;

@Ignore("Only abstract test class")
@SpringBootTest
@RunWith(SpringRunner.class)
public abstract class TestAbstract {

	// before all other to get database connection
	public static RabbitMQContainer rabbitMQContainer = new RabbitMQContainer("rabbitmq:3.7-management")
			.withExposedPorts(5672).withVhost("/");
	static {
		rabbitMQContainer.start();

		String propertyHost = "spring.rabbitmq.host";
		String host = rabbitMQContainer.getContainerIpAddress();
		System.setProperty(propertyHost, host);

		String propertyPort = "spring.rabbitmq.port";
		String port = rabbitMQContainer.getFirstMappedPort() + "";
		System.setProperty(propertyPort, port);
	}
}

package springrabbitmq;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import springrabbitmq.config.RabbitConfiguration;
import springrabbitmq.message.RabbitService;

public class RabbitServiceTest extends TestAbstract {

	@Autowired
	private RabbitService rabbitService;

	@Autowired
	private RabbitConfiguration configuration;

	@Test
	public void send_receive_test() {

		String queueName1 = configuration.getQueue1();
		String carl = "Hello Carl!";
		String horst = "Hello Horst!";
		rabbitService.send(queueName1, carl);
		rabbitService.send(queueName1, horst);

		String queueName2 = configuration.getQueue2();
		String tanya = "Hello Tanya!";
		String july = "Hello July!";
		rabbitService.send(queueName2, tanya);
		rabbitService.send(queueName2, july);

		assertThat(rabbitService.receive(queueName1)).isEqualTo(carl);
		assertThat(rabbitService.receive(queueName1)).isEqualTo(horst);
		assertThat(rabbitService.receive(queueName1)).isNotBlank();

		assertThat(rabbitService.receive(queueName2)).isEqualTo(tanya);
		assertThat(rabbitService.receive(queueName2)).isEqualTo(july);
		assertThat(rabbitService.receive(queueName2)).isNotBlank();

	}
}

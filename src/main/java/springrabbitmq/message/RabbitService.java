package springrabbitmq.message;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitService {

	@Autowired
	private RabbitTemplate template;

	public void send(String queueName, String content) {
		template.convertAndSend(queueName, content);
	}

	public String receive(String queueName) {
		return (String) template.receiveAndConvert(queueName);
	}
}

package springrabbitmq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class RabbitConfiguration {

	@Value("${spring.rabbitmq.queue1}")
	private String queue1;

	@Value("${spring.rabbitmq.queue2}")
	private String queue2;

	@Bean
	public Queue queue1() {
		return new Queue(queue1);
	}

	@Bean
	public Queue queue2() {
		return new Queue(queue2);
	}

}

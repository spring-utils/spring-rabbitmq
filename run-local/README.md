# Run RabbitMQ local

    docker-compose -f docker-compose-rabbitmq.yml up -d

# UI

http://localhost:15672

User: rabbitUser
PWD:  rabbitPwd

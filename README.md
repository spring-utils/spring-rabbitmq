# Spring RabbitMQ

Simple example to run spring-boot with [rabbit-mq](https://www.rabbitmq.com/tutorials/tutorial-one-spring-amqp.html) and for tests we use [test-containers](https://www.testcontainers.org/modules/rabbitmq/).

